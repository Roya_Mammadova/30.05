--task1
select e.employee_id,e.first_name,e.last_name,
d.department_name,j.job_title,
c.country_name
from employees e 
inner join departments d on e.department_id=d.department_id
left join jobs j on e.job_id=j.job_id
left join locations l on d.location_id=l.location_id
left join countries c on l.country_id=c.country_id;

--task2
select e.employee_id,e.first_name,e.last_name,
d.department_name,j.job_title,
c.country_name,jh.start_date,
jh.end_date
from employees e 
left join departments d on e.department_id=d.department_id
left join jobs j on e.job_id=j.job_id
left join locations l on d.location_id=l.location_id
left join countries c on l.country_id=c.country_id
left join job_history jh on e.employee_id=jh.employee_id;

--task3
select e.employee_id,e.first_name,e.last_name,
d.department_name,j.job_title,
c.country_name,d.department_id
from employees e 
right join departments d on e.department_id=d.department_id
left join jobs j on e.job_id=j.job_id
left join locations l on d.location_id=l.location_id
left join countries c on l.country_id=c.country_id;

--task4
select e.employee_id,e.first_name,e.last_name,
d.department_name,j.job_title,
c.country_name,d.department_id
from employees e 
left join departments d on e.department_id=d.department_id
right join jobs j on e.job_id=j.job_id
right join locations l on d.location_id=l.location_id
right join countries c on l.country_id=c.country_id;
--task5
select e.employee_id,e.first_name,e.last_name,
d.department_name,j.job_title,
c.country_name,d.department_id
from employees e 
inner join departments d on e.department_id=d.department_id
inner join jobs j on e.job_id=j.job_id
right join locations l on d.location_id=l.location_id
right join countries c on l.country_id=c.country_id;
